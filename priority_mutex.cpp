#include "priority_mutex.h"

#include <thread>
#include <sstream>
#include <iostream>

using namespace std;
using namespace lib::util;

PriorityMutex::PriorityMutex() {}


PriorityMutex::~PriorityMutex() {}


const unsigned long long PriorityMutex::load_current_tid()
{
    stringstream tid;
    tid << this_thread::get_id();
    return stoull( tid.str() );
}


void PriorityMutex::lock( priority pr )
{
    unsigned long long tid = load_current_tid();
    unique_lock<mutex> uLock(syncLock);

    try
    {
        enqueue_thread(pr);
        ThreadNode *runningNode = &sortedQueue.back();

        while( tid != runningNode->get_thread_id() )
        {
            threadIsNotRunner.wait(uLock);
            runningNode = &sortedQueue.back();
        }

        runningNode->increment_recursions_count();
        runningNode->set_thread_as_runner();
        accessLock.lock();
    }
    catch(...)
    {
        throw "Error in priority_mutex lock()";
    }
}


void PriorityMutex::unlock()
{
    unique_lock<mutex> uLock(syncLock);

    try
    {
        ThreadNode *runningNode = &sortedQueue.back();

        if( runningNode->decrement_recursions_count() == 0 )
        {
            dequeue_thread();
            accessLock.unlock();
            threadIsNotRunner.notify_all();
        }
        else
        {
            accessLock.unlock();
        }
    }
    catch(...)
    {
        throw"Error in priority_mutex unlock()";
    }
}


void PriorityMutex::enqueue_thread( priority pr )
{
    unsigned long long tid = load_current_tid();
    lock_guard<mutex> qGuard( queueLock );

    if( sortedQueue.empty() )
    {
        pr = RUNNER_PRIORITY;
    }
    else if( sortedQueue.back().get_thread_id() == tid )
    {
        return;
    }

    sortedQueue.push_front( ThreadNode(tid, pr) );

    auto nodeA = sortedQueue.begin();
    auto nodeB = nodeA++; // makes nodeA, current and nodeB next

    try
    {
        while( nodeA != sortedQueue.end() && nodeB->get_priority() > nodeA->get_priority() )
        {
            swap<ThreadNode>( *nodeB, *nodeA);
            nodeB = nodeA++;
        }
    }
    catch(...)
    {
        throw "Swapping error in enqueue_thread()";
    }
}


void PriorityMutex::dequeue_thread( void )
{
    lock_guard<mutex> qGuard(queueLock);

    if( !sortedQueue.empty() )
    {
        sortedQueue.pop_back();
    }
    else
    {
        throw "Removing from an empty list in dequeue_thread()";
    }
}
