#ifndef _LIB_PRIORITY_MUTEX
#define _LIB_PRIORITY_MUTEX

#include <list>
#include <mutex>
#include <string>
#include <condition_variable>


namespace lib
{
    namespace util
    {
        /**
         *@brief    Enum type to assign a thread its priority in obtaining this lock
         *
         */
        enum priority: int
        {
            LOW_PRIORITY=0,
            HIGH_PRIORITY,
            RUNNER_PRIORITY
        };

        /**
        * @brief    An implementation of a priority mutex that schedules the next thread
        *           with highest priority to acquire the lock.
        */
        class PriorityMutex
        {

            /**
             * @brief   An inner class Thread node to store each thread id,
             *          priority and the number of recursions
             */
            class ThreadNode
            {
            private:

                unsigned long long threadId;
                priority threadPriority;
                int recursions_count = 0;

            public:

                ThreadNode( unsigned long long tid, priority pr=LOW_PRIORITY )
                  : threadId(tid), threadPriority(pr)
                {}

                ~ThreadNode( void ) {}

                /**
                 * @brief   Gets the stored thread Id
                 * @return  The updated count of recursions
                 */
                const unsigned long long get_thread_id( void )
                {
                    return threadId;
                }

                /**
                 * @brief   Decrements the count of recursions
                 * @return  The updated count of recursions
                 */
                const int decrement_recursions_count( void )
                {
                    return --recursions_count;
                }

                /**
                 * @brief   Increments the count of recursions
                 * @return  The updated count of recursions
                 */
                const int increment_recursions_count( void )
                {
                    return ++recursions_count;
                }

                /**
                 * @brief   Gets the current count of recursions on this mutex
                 * @return  Integer count of recursions performed on this mutex
                 */
                const int get_recursions_count(void) { return recursions_count; }

                /**
                 * @brief   Gives the highest priority to the current thread so
                 *          it is not preempted during recursive locking
                 */
                void set_thread_as_runner( void ) { threadPriority = RUNNER_PRIORITY; }

                /**
                 * @brief   Sets the thread priority within inclusive bounds [LOW_PRIORITY ... HIGH_PRIORITY]
                 * @param   pr, the priority level the thread is trying to lock this lock with
                 */
                void set_priority( priority prty )
                {
                    priority pr = prty;
                    if( pr > HIGH_PRIORITY )
                    {
                        pr = HIGH_PRIORITY;
                    }
                    if( pr < LOW_PRIORITY )
                    {
                        pr = LOW_PRIORITY;
                    }

                    threadPriority = pr;
                }

                /**
                 * @brief   Gets the priority of this thread node
                 * @return  The thread priority
                 */
                const priority get_priority( void ) { return threadPriority; }
            };

        private:

            std::mutex   syncLock;
            std::mutex   queueLock;
            std::recursive_mutex   accessLock;
            std::condition_variable threadIsNotRunner;
            std::list<ThreadNode> sortedQueue;

            /**
             * @brief   Adds and entry with priority to the queue
             * @param   pr, the priority for the task
             */
            void enqueue_thread( priority pr );

            /**
             * @brief   pop the priority queue from front
             */
            void dequeue_thread( void) ;

        public:

            PriorityMutex(void);

            ~PriorityMutex(void);

            /**
             * @brief   Gets the ID of the thread requesting the lock
             * @return  unsigned long long ID of the thread
             *
             */
            const unsigned long long load_current_tid( void );

            /**
             * @brief   Locks this mutex
             * @param   pr, the priority to give to the current task
             */
            void lock( priority pr=LOW_PRIORITY );

            /**
             * @brief   Unlocks this mutex.
             */
            void unlock( void );
        };



        /**
         *@brief    A class to implement features of c++ lock_guard class
         *          Applicable to PriorityMutex objects only
         */
        class PriorityLockGuard
        {
        private:

            PriorityMutex *prLock;

        public:

            PriorityLockGuard( PriorityMutex &lock, priority pr=LOW_PRIORITY )
            {
                prLock = &lock;
                prLock->lock(pr);
            }

            ~PriorityLockGuard( void ) { prLock->unlock(); }
        };

    }

}

#endif
